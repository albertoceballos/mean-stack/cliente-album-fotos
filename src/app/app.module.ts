import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MomentModule} from 'angular2-moment';

import { AppComponent } from './app.component';
import { AlbumsListComponent } from './components/albums-list/albums-list.component';
import { HomeComponent } from './components/home/home.component';
import { AddAlbumComponent } from './components/add-album/add-album.component';
import { EditAlbumComponent } from './components/edit-album/edit-album.component';
import { AlbumDetailComponent } from './components/album-detail/album-detail.component';
import { AddImageComponent } from './components/add-image/add-image.component';
import { EditImageComponent } from './components/edit-image/edit-image.component';
import { DetailImageComponent } from './components/detail-image/detail-image.component';


var appRoutes:Routes=[
  {path:'album-list',component:AlbumsListComponent},
  {path:'inicio',component:HomeComponent},
  {path:'',component:HomeComponent},
  {path:'nuevo-album',component:AddAlbumComponent},
  {path:'editar-album/:idAlbum',component:EditAlbumComponent},
  {path:'album/:idAlbum',component:AlbumDetailComponent},
  {path:'nueva-imagen/:idAlbum',component:AddImageComponent},
  {path:'editar-imagen/:idImage',component:EditImageComponent},
  {path:'imagen/:idImage',component:DetailImageComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    AlbumsListComponent,
    HomeComponent,
    AddAlbumComponent,
    EditAlbumComponent,
    AlbumDetailComponent,
    AddImageComponent,
    EditImageComponent,
    DetailImageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    MomentModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
