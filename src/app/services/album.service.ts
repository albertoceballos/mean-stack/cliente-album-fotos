import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//URL global API
import { GLOBAL } from '../services/global';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {
  public url: string;
  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL.url;
  }

  //todos los albums
  getAlbums():Observable<any> {
    return this._httpClient.get(this.url+'albums');
  }

  //crear nuevo album
  addAlbum(album):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/json');
    let params=JSON.stringify(album);
    return this._httpClient.post(this.url+'album',params,{headers:headers});
  }

  //extraer álbum concreto
  getAlbum(idAlbum):Observable<any>{
    return this._httpClient.get(this.url+'album/'+idAlbum);
  }

  //actualizar album
  updateAlbum(album,idAlbum):Observable<any>{
    let headers=new HttpHeaders({'Content-Type':'application/json'});
    let params=JSON.stringify(album);

    return this._httpClient.put(this.url+'album/'+idAlbum,params,{headers:headers});
  }

  deleteAlbum(idAlbum):Observable<any>{
    return this._httpClient.delete(this.url+'album/'+idAlbum);
  }
}

