import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';

//servicios
import {AlbumService} from '../../services/album.service';
import {ImageService} from '../../services/image.service';

import {Album} from '../../models/Album';
import {Image} from '../../models/Image';

//url global API
import {GLOBAL} from '../../services/global';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.css']
})
export class AlbumDetailComponent implements OnInit {
  public pageTitle:string;
  public album:Album;
  public idAlbum:string;
  public imagesStatus:string;
  public message:string;
  public images:Array<Image>;
  public url:string;
  public status:string;
  constructor(private _router:Router,private _albumService:AlbumService, private _imageService:ImageService ,private _activatedRoute:ActivatedRoute) {
    this.pageTitle='Detalle del álbum';
    this.url=GLOBAL.url;
   }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        this.idAlbum=params['idAlbum'];
        this.getAlbum(this.idAlbum);
        this.getImagesAlbum(this.idAlbum);
      }
    );
  }

  getAlbum(id){
    this._albumService.getAlbum(id).subscribe(
      response=>{
        if(response.album){
          this.album=response.album;
          this.pageTitle=this.album.title;
        }else{
          this._router.navigate(['album-list']);
        }
      },
      error=>{
        console.log(error);
        this._router.navigate(['album-list']);
      }
    );
  }

  //sacar fotos del album
  getImagesAlbum(id){
    this._imageService.getImages(id).subscribe(
      response=>{
        if(response.images){
          console.log(response);
          this.images=response.images
          if(this.images.length>0){
            this.imagesStatus='success';
          }else{
            this.imagesStatus='error';
            this.message='No hay fotos en el álbum';  
          }
        
        }else{
          this.imagesStatus='error';
          this.message='Error al cargar las fotos';
        }
      },
      error=>{
        console.log(error);
        this.imagesStatus='error';
        this.message='Error en el servidor';
      }
    );
  }

  deleteImage(id){
    this._imageService.deleteImage(id).subscribe(
      response=>{
        console.log(response);
        if(response.image){
          alert('Foto borrada con éxito');
          this.getImagesAlbum(this.idAlbum);
        }else{
          this.status='error';
          this.message='Error al borrar la foto';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message='Error al borrar la foto'; 
      }
    );
  }

}
