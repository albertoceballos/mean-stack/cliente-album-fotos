import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//servicios
import { ImageService } from '../../services/image.service';

//modelos
import { Image } from '../../models/Image';

import {GLOBAL} from '../../services/global';
import { DISABLED } from '@angular/forms/src/model';

@Component({
  selector: 'app-add-image',
  templateUrl: './add-image.component.html',
  styleUrls: ['./add-image.component.css']
})
export class AddImageComponent implements OnInit {
  public pageTitle: string;
  public image: Image;
  public idAlbum: string;
  public url:string;
  public filesToUpload: Array<File>;
  public resultUpload;
  public status:string;
  public message:string;
  constructor(private _activatedRoute: ActivatedRoute, private _imageService: ImageService) {
    this.pageTitle = 'Añadir nueva imagen';
    this.image = new Image('', '', '', '', '');
    this.url=GLOBAL.url;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        this.idAlbum = params['idAlbum'];
        this.image.album = this.idAlbum;
      }
    );
  }

  fileChangeEvent(fileInput) {
    this.filesToUpload = fileInput.target.files;
  }

  //subida de imagen
  makeFileRequest(url, params, files) {
    return new Promise((resolve, reject) => {
      var formData = new FormData();
      var xhr = new XMLHttpRequest();
      for (let i = 0; i < files.length; i++) {
        formData.append('image', files[i], files[i].name);
      }
      xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
          if(xhr.status==200){
            resolve(JSON.parse(xhr.response));
          }else{
            reject(xhr.response);
          }
        }
      }
      xhr.open('POST',url,true);
      xhr.send(formData);
    });
  }


  onSubmit() {
    console.log(this.image);
    this._imageService.saveImage(this.image).subscribe(
      response => {
        if(response.image){
          this.image=response.image;
          var imageId=this.image._id;
          this.makeFileRequest(this.url+'upload-image/'+imageId,[],this.filesToUpload)
          .then(
            (result)=>{
              this.resultUpload=result;
              console.log(this.resultUpload);
              this.status='success';
              this.message=this.resultUpload.message;
              setTimeout(()=>{
                this.status=null;
              },1500);
            },
            (error)=>{
              console.log(error);
              this.message='Error al subir la imagen';
            }
          );
        }else{
          this.status='error';
        }
      },
      error => {
        console.log(error);
        this.status='error';
        this.message='Error al guardar la imagen';
      }
    );
  }

}
