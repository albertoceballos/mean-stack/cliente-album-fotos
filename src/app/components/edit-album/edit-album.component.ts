import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {AlbumService} from '../../services/album.service';

import {Album} from '../../models/Album';

@Component({
  selector: 'app-edit-album',
  templateUrl: '../add-album/add-album.component.html',
  styleUrls: ['./edit-album.component.css']
})
export class EditAlbumComponent implements OnInit {
  public pageTitle:string;
  public album:Album;
  public message:string;
  public status:string;
  public albumId:string;
  constructor(private _albumService:AlbumService, private _activatedRoute:ActivatedRoute,private _router:Router) {
    this.pageTitle="Editar álbum";
    this.album=new Album('','','');
   }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        this.albumId=params['idAlbum'];
        this.getAlbum(this.albumId);
      }
    );
  }

  getAlbum(id){
    this._albumService.getAlbum(id).subscribe(
      response=>{
        if(response.album){
          this.album=response.album;
        }else{
          this._router.navigate(['album-list']);
        }
      },
      error=>{
        console.log(error);
        this._router.navigate(['album-list']);
      }
    );
  }

  onSubmit(){
    this._albumService.updateAlbum(this.album,this.albumId).subscribe(
      response=>{
        if(response.album){
          this.status='success';
          this.message=response.message;
        }else{
          this.status='error';
          this.message='No se ha podido actualizar el álbum';  
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        if(error.error.message){
          this.message=error.error.message;
        }else{
          this.message='Error al actualizar el álbum';
        }
      }
    );
  }

}
