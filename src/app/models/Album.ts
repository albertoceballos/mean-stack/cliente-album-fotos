export class Album{
    public _id:number;
    public title:string;
    public description:string;

    constructor(_id,title,description){
        this._id=_id;
        this.title=title;
        this.description=description;

    }
}